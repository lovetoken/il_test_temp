Install
-------

    library(remotes)
    install_gitlab("lovetoken/intelligencelabs", host = "nexon.gitlab.com")

Function lists
--------------

    ls("package:intelligencelabs")

    ## [1] "retention"

License
=======

[GPL-3](https://www.gnu.org/licenses/gpl-3.0.en.html)
